In here you can find a dedicated server build with a map you can walk in. 
You open the server by going to the directory in this repository: WindowsNoEditor\Comm_CharlieFoxtrot\Binaries\Win64 
First you open the Comm_CharlieFoxtrotServer.exe - Shortcut ( because this comes with a log ).

You have to wait until the server is up and running and you see this message:

![Imgur](https://imgur.com/z04OG08.png)
Then you can start the Comm_CharlieFoxtrot.exe.

![Imgur](https://imgur.com/w1nxRSb.png)

THIS ONLY WORKS ON YOUR LOCAL MACHINE RIGHT NOW.